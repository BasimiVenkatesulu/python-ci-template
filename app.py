
class Calculator:
    @staticmethod
    def add(a, b):
        return a + b

    def subtract(a, b):
        return a - b

    def multiply(a, b):
        return a * b

    def divide(a, b):
        return float(a) / b  

if __name__ == '__main__':
    a = 5
    b = 7
    print(f"     Add {a, b}: {Calculator.add(a, b)}") 
    print(f"Subtract {a, b}: {Calculator.subtract(a, b)}") 
    print(f"Multiply {a, b}: {Calculator.multiply(a, b)}") 
    print(f"  Divide {a, b}: {Calculator.divide(a, b)}") 
